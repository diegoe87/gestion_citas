<?php
namespace Drupal\gestion_citas\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
/**
 * Class ListaCitasController.
 *
 * @package Drupal\gestion_citas\Controller
 */
class ListaCitasController extends ControllerBase {
  
  public function getContent() {
    // Información para el usuario
    $build = [
      'description' => [
        '#theme' => 'gestion_citas_descripcion',
        '#description' => 'Gestión Citas',
        '#attributes' => [],
      ],
    ];
    return $build;
  }

  /**
  * Display.
  *
  * @return string
  * Muestra una tabla con las citas
  */
  public function display() {
    //Definicion de la tabla y encabezado
    $header_table = array(
      'id'=>    t('ID'),
      'nombre' => t('Nombre'),
      'documento' => t('Documento'),
      'email'=>t('Email'),
      'fecha_hora' => t('Fecha y Hora'),
      'descripcion' => t('Descripción'),
      'opt' => t('Editar'),
      'opt1' => t('Eliminar'),
    );
    //Trae los registros de la base de datos
    $query = \Drupal::database()->select('gestion_citas', 'gc');
    $query->fields('gc', ['id','nombre','documento','email','fecha_hora','descripcion']);
    $results = $query->execute()->fetchAll();
    $rows=array();
    foreach($results as $data){
      $delete = Url::fromUserInput('/gestion-citas/'.$data->id.'/edit');
      $edit   = Url::fromUserInput('/gestion-citas/'.$data->id.'/edit');
      //Imprime la informacion en la tabla
       $rows[] = array(
        'id' =>$data->id,
        'nombre' => $data->nombre,
        'documento' => $data->documento,
        'email' => $data->email,
        'fecha_hora' => $data->fecha_hora,
        'descripcion' => $data->descripcion,
        \Drupal::l('Edit', $edit),
        \Drupal::l('Delete', $delete)
      );
    }
    //Muestra los datos en la pagina
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header_table,
      '#rows' => $rows,
      '#empty' => t('No hay citas'),
    ];
    return $form;
  }
}