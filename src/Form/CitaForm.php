<?php
/**
 * @file
 * Contains \Drupal\gestion_citas\Form\CitaForm.
 */

namespace Drupal\gestion_citas\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Fromualario para la creacion de citas
 */
class CitaForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gestion_citas_crear_form';
  }

  /**
   * {@inheritdoc}
   * Declaración de los campos del formulario para la creación y edición de citas
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['nombre'] = array(
      '#type' => 'textfield',
      '#title' => t('Nombre Persona'),
      '#required' => TRUE,
    );
    $form['documento'] = array(
      '#type' => 'number',
      '#title' => t('Documento'),
      '#required' => TRUE,
    );
    $form['email'] = array(
      '#type' => 'email',
      '#title' => t('Email'),
      '#required' => TRUE,
    );
    $form['fecha_hora'] = array(
      '#type' => 'datetime',
      '#title' => t('Fecha y Hora'),
      '#required' => TRUE,
    );
    $form['descripcion'] = array(
      '#type' => 'textarea',
      '#title' => t('Descripcion'),
    );
    $form['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   * Validacion de campos
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validar fecha de la cita.
    if ( strtotime($form_state->getValue('fecha_hora')) < time() ) {
      $form_state->setErrorByName('fecha_hora', $this->t("La fecha y hora de la cita debe ser en el futuro."));
    }
  }

  /**
   * {@inheritdoc}
   * Guardar la información en la base de datos.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Recuperamos la información de los campos
    $nombre = $form_state->getValue('nombre');
    $documento = $form_state->getValue('documento');
    $email = $form_state->getValue('email');
    $fecha_hora = $form_state->getValue('fecha_hora');
    $descripcion = $form_state->getValue('descripcion');
    $field = array(
        'nombre' =>  $nombre,
        'documento' => $documento,
        'email' =>  $email ,
        'fecha_hora' => $fecha_hora,
        'descripcion' =>  $descripcion,
    );
    db_insert('gestion_citas')
        ->fields($field)
        ->execute();
    drupal_set_message($this->t("Cita guardada!"));
  }
}
?>